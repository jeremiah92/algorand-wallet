import algosdk from "algosdk";
import { uuid } from "uuidv4";

if (!window.algo) {
  // only provide if none exists
  const apiToken =
    "0f24cac92e5ead6afbcf389e0ade28bb609d24ca6687359f342748c68d6cf9b2";
  const server = "https://indexer.algorand.network";
  const port = 8443;
  window.algo = new algosdk.Algod(apiToken, server, port);
  window.algosdk = algosdk;
}

let callbacks = {};

window.addEventListener(
  "ALGO_PASS_TO_SCRIPT",
  ({ target, detail }) => {
    if (target != window) {
      return;
    }
    const callback = callbacks[detail.id];
    if (callback) {
      callback(detail.response);
      delete callbacks[detail.id];
    }
  },
  false
);

const sendMessage = payload =>
  new Promise(resolve => {
    const id = uuid();
    callbacks[id] = response => resolve(response);
    const event = new CustomEvent("ALGO_PASS_TO_BACKGROUND", {
      detail: { id, payload }
    });
    window.dispatchEvent(event);
  });

window.algo.connect = async () => {
  return await sendMessage({ type: "CONNECT" });
};

window.algo.sendTransaction = async opts => {
  const response = await sendMessage({
    type: "INJECTED_SEND_TRANSACTION",
    ...opts
  });
  if (response.error) {
    throw new Error(response.error);
  }
  return response;
};

console.log("Algo Provider Injected ");
