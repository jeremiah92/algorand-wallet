const path = require("path");

module.exports = {
  entry: {
    background: "./js/background",
    content: "./js/content",
    provider: "./js/provider"
  },
  output: {
    path: path.resolve(__dirname, "../build"),
    filename: "[name].js"
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  mode: "production"
};
