import React from "react";

export default ({ value, children, style }) => (
  <div
    style={{ cursor: "pointer", ...style }}
    onClick={() => {
      const el = document.createElement("textarea");
      el.value = value;
      document.body.appendChild(el);
      el.select();
      document.execCommand("copy");
      document.body.removeChild(el);
    }}
  >
    {children}
    <i style={{ marginLeft: 10 }} className="far fa-copy"></i>
  </div>
);
