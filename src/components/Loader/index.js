import React from "react";
import Center from "../Center";

export default () => (
  <Center>
    <div key={0} className="spinner-grow m-3" role="status">
      <span className="sr-only">Loading...</span>
    </div>
  </Center>
);
